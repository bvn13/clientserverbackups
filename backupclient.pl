#!/usr/bin/perl

use strict;
use warnings;

use Data::Dumper;

use Mojo::UserAgent;

#########################################################################

sub say { print Dumper @_; }

#my $settings = {
#	'GTV823' => {
#
#	}
#
#};

my $settings = {

	#'bases' => ['GTV823', 'ZAKR_2012'],
	'bases' => ['incident'],

	'execpath' => 'C:\\',

	'execbat' => '"C:\\Program Files\\Microsoft SQL Server\\90\\Tools\\Binn\\SQLCMD.EXE" -S SUN -U sa -P asSA1 -i !FILE!',

	'IPfile' => '\\\\SUN\\bupconf\\ip',

	'sqlscript_def' => <<EOS

		DECLARE \@MyBackupName nvarchar(500) 
		SET \@MyBackupName = 'E:\\Backups\\!BASENAME!\\!BASENAME!_backup_' + CONVERT(VARCHAR(10),GETDATE(),112) + SUBSTRING(CONVERT(VARCHAR(16),GETDATE(),126),12,2) + SUBSTRING(CONVERT(VARCHAR(16),GETDATE(),126),15,2) + '.bak'
		BACKUP DATABASE [!BASENAME!] TO DISK = \@MyBackupName
			WITH 
			NOFORMAT, 
			NOINIT, 
			NAME = N'!BASENAME! - FULL Database Backup (!DATETIME!)',
			SKIP, 
			NOREWIND, 
			NOUNLOAD, 
			STATS = 10
		GO

EOS

};


sub getCurrTime
{
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime;
	my @abbr = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );

	$year+=1900;
	my $date = "$year $abbr[$mon] $mday $hour:$min:$sec";

	@abbr = ();
	undef @abbr;
	undef $sec;
	undef $min;
	undef $hour;
	undef $mday;
	undef $mon;
	undef $year;
	undef $wday;
	undef $yday;
	undef $isdst;

return $date;
}


sub noticeServer {

	my $dbname = shift;

	open FIP, "< ".$settings->{IPfile} or do { print "Could not open IP-file!\n"; return; };

	my $ip = <FIP>;
	chomp $ip;
	close FIP;

	if ($ip =~ m/^\d{1,3}(\.\d{1,3}){3,3}$/) {
		my $ua = Mojo::UserAgent->new;
		my $tx = $ua->get("http://$ip:3000/do/$dbname");
		my $result = $tx->res->json or do { print "An uncorrect answer was received from server!\n" . $tx->res->content; return; };

		print "DB: $result->{db}\nRESULT: $result->{status}\n";

	} else {
		print "IP $ip does not match!\n";
	}
}


########################################################################################################
for my $dbname (@{$settings->{bases}}) {

	print "Making a backup of <$dbname>...\n";

	my $sql = $settings->{sqlscript_def};

	my $currDateTime = getCurrTime;

	$sql =~ s/!BASENAME!/$dbname/gi;
	$sql =~ s/!DATETIME!/$currDateTime/gi;

	#print "$sql\n";

	my $fname = $settings->{execpath} . $dbname . '.sql';

	open TMP, "> $fname";
	print TMP $sql;
	close TMP;

	print "$fname\n";

	my $exec = $settings->{execbat};

	$exec =~ s/!FILE!/$fname/gi;

	print "EXECBAT: $exec\n";

	my $result = `$exec`;

	print "$result\n";

	if ($result =~ m/BACKUP DATABASE successfully processed/gi) {
		print "\nOK\n";
		noticeServer $dbname;
	} else {
		print "\nERROR\n";
	}
}

#TODO: при запуске надо очищать папку, где хранится старый бекап